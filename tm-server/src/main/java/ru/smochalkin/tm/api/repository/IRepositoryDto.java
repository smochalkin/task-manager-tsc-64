package ru.smochalkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.dto.AbstractEntityDto;

import javax.persistence.EntityManager;

public interface IRepositoryDto<E extends AbstractEntityDto> {

    void add(@NotNull E entity);

    void update(E entity);

    EntityManager getEntityManager();

}
