package ru.smochalkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractSystemListener;
import ru.smochalkin.tm.util.NumberUtil;

@Component
public final class InfoListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String arg() {
        return "-i";
    }

    @Override
    @NotNull
    public String name() {
        return "info";
    }

    @Override
    @NotNull
    public String description() {
        return "Display system information.";
    }

    @Override
    @EventListener(condition = "@infoListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[INFO]");
        @NotNull final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        @NotNull final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
