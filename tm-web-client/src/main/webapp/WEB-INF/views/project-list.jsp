<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Status</th>
        <th>Created</th>
        <th>Finish date</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.status.getDisplayName()}"/>
            </td>
            <td>
                <fmt:formatDate value="${project.created}" pattern="dd.MM.yyyy"/>
            </td>
            <td>
                <fmt:formatDate value="${project.endDate}" pattern="dd.MM.yyyy"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <a href="/project/edit/${project.id}">Edit</a>
            </td>
            <td>
                <a href="/project/delete/${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>
<form action="/project/create">
    <button type="submit">Create</button>
</form>
<jsp:include page="../include/_footer.jsp"/>