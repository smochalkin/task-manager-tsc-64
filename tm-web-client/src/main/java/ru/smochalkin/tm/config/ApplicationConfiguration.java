package ru.smochalkin.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.smochalkin.tm")
public class ApplicationConfiguration {
}
