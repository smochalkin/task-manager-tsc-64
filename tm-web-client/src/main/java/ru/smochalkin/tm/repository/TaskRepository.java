package ru.smochalkin.tm.repository;

import org.springframework.stereotype.Repository;
import ru.smochalkin.tm.model.Task;

import java.util.*;

@Repository
public class TaskRepository {

    private Map<String, Task> tasks = new HashMap<>();

    {
        add("task 1");
        add("task 2");
        add("task 3");
    }

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void add(final String name) {
        final Task task = new Task(name);
        tasks.put(task.getId(), task);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

    public List<Task> findAll() {
        return new ArrayList<>(tasks.values()) ;
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

}
